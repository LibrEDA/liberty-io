<!--
SPDX-FileCopyrightText: 2022 Thomas Kramer

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Liberty-IO

Liberty-io is a Rust crate for parsing and writing the 'liberty' format which is used for characterizing the timing behaviour of digital CMOS standard-cells.

Since liberty files can be huge (up to gigabytes) this parser does not require to load the full file into memory but streams it from the disk.

# Example

Read a liberty file:
```rust
use liberty_io;
use std::fs::File;
use std::io::BufReader;

// Create a buffered reader for faster reading.
let f = File::open("./tests/data/freepdk45/gscl45nm.lib").expect("Failed to open file.");
let mut buf = BufReader::new(f);

// Read the file.
let read_result = liberty_io::read_liberty_bytes(&mut buf);

// Abort the program if the library could not be read.
let library_group = read_result.expect("Failed to read library!");

// Access the content.
assert_eq!(&library_group.name, "library");
assert_eq!(&library_group.arguments[0].to_string(), "gscl45nm");

// List all cell names. (There's only DFFNEGX1 in the provided example file.)
println!("Library cells:");
for group in &library_group.groups {
    if group.name == "cell" {
        println!("* {}", &group.arguments[0]);
    }   
}

// There's some utility functions for accessing the library structure.
// Find a cell group by it's name.
let dffnegx1 = liberty_io::util::find_cell(&library_group, "DFFNEGX1");
assert!(dffnegx1.is_some());


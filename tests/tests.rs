// Copyright (c) 2021-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#![cfg(test)]

use liberty_io;

use liberty_io::{AttributeType, Define, Value};
use std::fs::File;
use std::io::BufReader;

#[test]
fn test_read_liberty_simple() {
    let data = r#"
library(test) {
  time_unit: 1ns;
  string: "asdf";
  mygroup(a, b) {}
  empty() {}
  somegroup(a, b, c) {
    nested_group(d, e) {
        simpleattr_float: 1.2;
    }
  }
  simpleattr_int : 1;
  complexattr(a, b);
  define(myNewAttr, validinthisgroup, float);
  pin(A[25]) {}
  pin(B[32:0]) {}
}
"#;

    let result = liberty_io::read_liberty_chars(data.chars());

    dbg!(&result);

    assert!(result.is_ok());
}

#[test]
fn test_read_bitslice() {
    let data = r#"
library(b, b[0], b[1:0]) {
}
"#;

    let result = liberty_io::read_liberty_chars(data.chars());
    dbg!(&result);
    assert!(result.is_ok());

    let library = result.unwrap();
    use liberty_io::Value;
    assert_eq!(
        library.arguments,
        vec![
            Value::String("b".to_string()),
            Value::Indexed("b".to_string(), 0),
            Value::Sliced("b".to_string(), 1, 0),
        ]
    );
}

#[test]
fn test_read_multiline() {
    let data = r#"
table(table_name2){
    str: "asd\
    f";
    index_1("1, 2, 3, 4, 5, 6, 7, 8");
    value("0001, 0002, 0003, 0004, \
    0005, 0006, 0007, 0008");
}
    "#;

    let result = liberty_io::read_liberty_chars(data.chars());
    dbg!(&result);
    assert!(result.is_ok());
}

#[test]
fn test_read_complex_attributes() {
    let data = r#"
group(test){
    define_group(g1, x);
    define_group(g2, z);
    voltage_map(VDD, 1.0);
    voltage_map(VSS, 0.0);
}
    "#;

    let result = liberty_io::read_liberty_chars(data.chars());
    dbg!(&result);
    assert!(result.is_ok());
    let group = result.unwrap();
    assert_eq!(
        group.attributes["voltage_map"][0],
        vec![Value::String("VDD".to_string()), Value::Float(1.0)]
    );
    assert_eq!(
        group.attributes["voltage_map"][1],
        vec![Value::String("VSS".to_string()), Value::Float(0.0)]
    );
}

#[test]
fn test_read_float() {
    let data = r#"
group(test){
    float1: 1.0;
    float2: 1.e-6;
    float3: 2.0E-6;
}
    "#;

    let result = liberty_io::read_liberty_chars(data.chars());
    dbg!(&result);
    assert!(result.is_ok());
    let group = result.unwrap();
    assert_eq!(group.attributes["float1"][0], vec![Value::Float(1.0)]);
    assert_eq!(group.attributes["float2"][0], vec![Value::Float(1e-6)]);
    assert_eq!(group.attributes["float3"][0], vec![Value::Float(2e-6)]);
}

#[test]
fn test_read_float_with_unit() {
    let data = r#"
library(test) {
  time_unit: 1ns;
}
    "#;

    let result = liberty_io::read_liberty_chars(data.chars());
    dbg!(&result);
    assert!(result.is_ok());
    let group = result.unwrap();
    // TODO: Parse into `Value::WithUnit(1.0, Unit::NanoSecond)`
    assert_eq!(
        group.attributes["time_unit"][0],
        vec![Value::String("1ns".to_string())]
    );
}

#[test]
fn test_read_state_table_multiline() {
    // From https://codeberg.org/tok/liberty-parser/issues/6
    let data = r#"
statetable ("CK E SE","IQ") {
	     table : "L L L : - : L ,\
	              L L H : - : H ,\
	              L H L : - : H ,\
	              L H H : - : H ,\
	              H - - : - : N " ;
	}
"#;

    let result = liberty_io::read_liberty_chars(data.chars());

    dbg!(&result);

    assert!(result.is_ok());

    let table = result.unwrap();
    assert_eq!(table.name, "statetable");
    assert_eq!(table.arguments[0].to_string(), "\"CK E SE\"");
    assert_eq!(table.arguments[1].to_string(), "\"IQ\"");
    assert!(table.attributes.get("table").is_some());
}

/// Test that multiple attributes with the same name don't overwrite each other.
/// See: https://codeberg.org/tok/liberty-parser/issues/7
#[test]
fn test_wire_load_model() {
    let data = r#"
    wire_load("1K_hvratio_1_4") {
        capacitance : 1.774000e-01;
        resistance : 3.571429e-03;
        slope : 5.000000;
        fanout_length( 1, 1.3207 );
        fanout_length( 2, 2.9813 );
        fanout_length( 3, 5.1135 );
        fanout_length( 4, 7.6639 );
        fanout_length( 5, 10.0334 );
        fanout_length( 6, 12.2296 );
        fanout_length( 7, 19.3185 );
    }
    "#;

    let result = liberty_io::read_liberty_chars(data.chars());
    dbg!(&result);
    assert!(result.is_ok());
    let group = result.unwrap();

    assert_eq!(group.attributes["fanout_length"].len(), 7);

    assert_eq!(
        group.attributes["fanout_length"][0],
        vec![Value::Integer(1), Value::Float(1.3207)]
    );
    assert_eq!(
        group.attributes["fanout_length"][6],
        vec![Value::Integer(7), Value::Float(19.3185)]
    );
}

#[test]
fn test_complex_attribute_without_semicolon() {
    let data = r#"
    library() {
        cplx1(1, 2, 3)
        cplx2(4, 5);
        cplx3(6)
    }
    "#;

    let result = liberty_io::read_liberty_chars(data.chars());
    dbg!(&result);
    assert!(result.is_ok());
}

#[test]
fn test_define() {
    let data = r#"
    library() {
        define("attr1", "groupname", float);
        define(attr2, groupname, string);
        define(attr3, "groupname", "integer");
    }
    "#;

    let result = liberty_io::read_liberty_chars(data.chars());
    dbg!(&result);
    assert!(result.is_ok());
    let lib = result.unwrap();
    assert_eq!(lib.defines.len(), 3);
    assert_eq!(
        lib.defines[0],
        Define {
            attribute_name: "attr1".to_string(),
            group_name: "groupname".to_string(),
            attribute_type: AttributeType::Float
        }
    );

    assert_eq!(lib.defines[1].attribute_name, "attr2");
}

/// Load the stripped-down timing liberty file of the FreePDK45.
#[test]
fn test_read_liberty_freepdk45() {
    let f = File::open("./tests/data/freepdk45/gscl45nm.lib").unwrap();
    let mut buf = BufReader::new(f);
    let result = liberty_io::read_liberty_bytes(&mut buf);

    dbg!(&result);

    assert!(result.is_ok());

    let library = result.unwrap();
    assert_eq!(library.name.to_string(), "library");
    assert_eq!(library.arguments[0].to_string(), "gscl45nm");
}

/// Load a cell timing liberty file of Sky130.
#[test]
fn test_read_liberty_sky130_ccs_noise() {
    let f =
        File::open("./tests/data/sky130/sky130_fd_sc_hs__bufinv_8__tt_1p80V_25C_ccsnoise.cell.lib")
            .unwrap();
    let mut buf = BufReader::new(f);

    let result = liberty_io::read_liberty_bytes(&mut buf);

    dbg!(&result);

    assert!(result.is_ok());

    let library = result.unwrap();
    assert_eq!(library.name.to_string(), "library");
}

/// Load a shortened cell timing liberty file from IHP-Open-PDK SG13G2.
#[test]
fn test_read_liberty_ihp_sg13g2() {
    let f =
        File::open("./tests/data/ihp-sg13g2/sg13g2_stdcell_fast_1p65V_m40C.shortened.lib").unwrap();
    let mut buf = BufReader::new(f);

    let result = liberty_io::read_liberty_bytes(&mut buf);

    dbg!(&result);

    assert!(result.is_ok());

    let library = result.unwrap();
    assert_eq!(library.name.to_string(), "library");
}

#[test]
fn test_read_operating_conditions() {
    let data = r#"
    library() {
        operating_conditions (1V10SS125C) {
        
        }
    }
    "#;

    let result = liberty_io::read_liberty_chars(data.chars());
    dbg!(&result);
    assert!(result.is_ok());
    let library = result.unwrap();
    assert_eq!(library.groups[0].arguments[0].to_string(), "1V10SS125C");
}

#[test]
fn test_read_minus_in_library_name() {
    let data = r#"
    library(some_lib_-30C) {
    }
    "#;

    let result = liberty_io::read_liberty_chars(data.chars());
    dbg!(&result);
    assert!(result.is_ok());
    let library = result.unwrap();
    assert_eq!(library.arguments[0].to_string(), "some_lib_-30C");
}

#[test]
fn test_read_colon_in_group_argument() {
    let data = r#"
    library(name:with:colon) {
    }
    "#;

    let result = liberty_io::read_liberty_chars(data.chars());
    dbg!(&result);
    assert!(result.is_ok());
    let library = result.unwrap();
    assert_eq!(library.arguments[0].to_string(), "name:with:colon");
}

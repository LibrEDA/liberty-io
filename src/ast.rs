// Copyright (c) 2021-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Structs for the representation of the liberty library.

use std::collections::BTreeMap;
use std::fmt;
use std::str::FromStr;

/// 'Group' in the liberty file.
///
/// A group has the form:
///
/// ```txt
/// group_name (attributes, ...) {
///     attributes ...
///     sub_groups ...
///     defines ...
/// }
/// ```
#[derive(Debug, Clone, Default)]
pub struct Group {
    /// Name of this group.
    pub name: String,
    /// Arguments of this group.
    pub arguments: Vec<Value>,
    /// Member attributes.
    /// An attribute with the same name can appear multiple times. Therefore
    /// the attributes are stored as a nested list like `vec![vec![attribute1, attribute2], ...]`
    pub attributes: BTreeMap<String, Vec<Vec<Value>>>,
    /// Sub-groups.
    pub groups: Vec<Group>,
    /// Defines statements in this group.
    pub defines: Vec<Define>,
}

/// Data type of attributes. This is used in define statements.
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum AttributeType {
    /// Boolean value.
    Boolean,
    /// String value.
    String,
    /// Integer number.
    Integer,
    /// Float number.
    Float,
}

impl fmt::Display for AttributeType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            AttributeType::Boolean => "boolean",
            AttributeType::String => "string",
            AttributeType::Integer => "integer",
            AttributeType::Float => "float",
        };
        write!(f, "{}", s)
    }
}

impl FromStr for AttributeType {
    type Err = ();

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        match input {
            "boolean" => Ok(AttributeType::Boolean),
            "string" => Ok(AttributeType::String),
            "integer" => Ok(AttributeType::Integer),
            "float" => Ok(AttributeType::Float),
            _ => Err(()),
        }
    }
}

/// Liberty `define` statement.
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Define {
    /// Name of the new defined attribute.
    pub attribute_name: String,
    /// Name of the group in which the attribute is created.
    pub group_name: String,
    /// Data type of the attribute: boolean, string, integer or float.
    pub attribute_type: AttributeType,
}

impl fmt::Display for Define {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "define ({}, {}, {})",
            self.attribute_name, self.group_name, self.attribute_type
        )
    }
}

/// Liberty values.
#[derive(Debug, Clone, PartialEq)]
pub enum Value {
    /// Boolean value: `true` or `false`.
    Bool(bool),
    /// A floating point scalar value.
    Float(f64),
    /// A floating point scalar value.
    Integer(i32),
    /// An array of floating point numbers: `"1.1, 1.2, 1.3"`
    FloatArray(Vec<f64>),
    /// An unquoted string.
    String(String),
    /// A quoted string like `"some string"`.
    QuotedString(String),
    /// A bit selection of the form `name[end]`.
    Indexed(String, u32),
    /// A bit slice of the form `name[end:start]`.
    Sliced(String, u32, u32),
}

impl Value {
    /// Try to get a `&str` value. Returns `None` if the value is not a `Value::String` or a `Value::QuotedString`.
    pub fn as_str(&self) -> Option<&str> {
        match self {
            Value::String(s) => Some(s.as_str()),
            Value::QuotedString(s) => Some(s.as_str()),
            _ => None,
        }
    }

    /// Try to get a `bool` value.
    pub fn as_bool(&self) -> Option<bool> {
        match self {
            Value::Bool(b) => Some(*b),
            _ => None,
        }
    }

    /// Try to get a `Vec<f64>` value.
    pub fn as_float_array(&self) -> Option<&Vec<f64>> {
        match self {
            Value::FloatArray(f) => Some(f),
            _ => None,
        }
    }

    /// Try to get a `f64` value.
    /// Converts integer values to floats.
    pub fn as_float(&self) -> Option<f64> {
        match self {
            Value::Float(f) => Some(*f),
            Value::Integer(i) => Some(*i as f64),
            _ => None,
        }
    }
    /// Try to get a `i32` value.
    pub fn as_integer(&self) -> Option<i32> {
        match self {
            Value::Integer(i) => Some(*i),
            _ => None,
        }
    }
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Value::Bool(b) => write!(f, "{:?}", b)?,
            Value::Float(v) => write!(f, "{}", v)?,
            Value::FloatArray(values) => {
                write!(f, "\"")?;
                for (i, v) in values.iter().enumerate() {
                    if i == values.len() {
                        write!(f, "{}", v)?;
                    } else {
                        write!(f, "{}, ", v)?;
                    }
                }
                write!(f, "\"")?;
            }
            Value::String(s) => write!(f, "{}", s)?,
            Value::QuotedString(s) => write!(f, r#""{}""#, s)?,
            Value::Indexed(name, idx) => write!(f, r#""{}[{}]""#, name, idx)?,
            Value::Sliced(name, end, start) => write!(f, r#""{}[{}:{}]""#, name, end, start)?,
            Value::Integer(i) => write!(f, "{}", i)?,
        };

        Ok(())
    }
}

// Copyright (c) 2021-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::ast::*;
use crate::ast_generated::*;
use crate::stream_lexer::*;
use crate::units::*;
use itertools::PeekingNext;
use libreda_stream_parser::tokenize;
use std::collections::BTreeMap;
use std::io::Read;
use std::iter::FromIterator;
use std::str::FromStr;

type Tokenized<I> = libreda_stream_parser::Tokenized<I, LibertyLexer>;

/// Definition of signal thresholds which define delay and slew measurements.
#[derive(Debug, Clone)]
pub struct TripPoints {
    slew_upper_threshold_pct_rise: f64,
    slew_lower_threshold_pct_rise: f64,
    slew_upper_threshold_pct_fall: f64,
    slew_lower_threshold_pct_fall: f64,
    /// Delay measurement threshold for rising input signals.
    input_threshold_pct_rise: f64,
    /// Delay measurement threshold for falling input signals.
    input_threshold_pct_fall: f64,
    /// Delay measurement threshold for rising output signals.
    output_threshold_pct_rise: f64,
    /// Delay measurement threshold for falling output signals.
    output_threshold_pct_fall: f64,

    /// Number between 0.0 and 1.0.
    slew_derate_from_library: f64,
}

impl Default for TripPoints {
    fn default() -> Self {
        Self {
            slew_upper_threshold_pct_rise: 80.0,
            slew_lower_threshold_pct_rise: 20.0,
            slew_upper_threshold_pct_fall: 80.0,
            slew_lower_threshold_pct_fall: 20.0,
            input_threshold_pct_rise: 50.0,
            input_threshold_pct_fall: 50.0,
            output_threshold_pct_rise: 50.0,
            output_threshold_pct_fall: 50.0,
            slew_derate_from_library: 1.0,
        }
    }
}

/// Physical unit definitions.
#[derive(Debug, Clone)]
pub struct Units {
    time_unit: TimeUnit,
    voltage_unit: (),
    current_unit: (),
    pulling_resistance_unit: (),
    capacitive_load_unit: (),
    leakage_power_unit: (),
}

impl Default for Units {
    fn default() -> Self {
        Self {
            time_unit: TimeUnit::Ns1,
            voltage_unit: (),
            current_unit: (),
            pulling_resistance_unit: (),
            capacitive_load_unit: (),
            leakage_power_unit: (),
        }
    }
}

/// Liberty library.
#[derive(Debug, Clone)]
pub struct Library {
    /// Name of the library.
    name: String,
    /// Name of the technology. Should be `cmos`.
    technology: String,
    delay_model: DelayModel,
    /// Name format of bus pins. Something like: `%s_%d` where `%s` is the
    /// name of the bus and `%d` is the number of the pin.
    bus_naming_style: String,

    revision: Option<Value>,
    comment: Option<Value>,
    date: Option<Value>,

    units: Units,

    /// Definition of signal thresholds which define delay and slew measurements.
    trip_points: TripPoints,

    nom_process: f64,
    nom_voltage: f64,
    nom_temperature: f64,

    /// Specify if the cells are for Silicon-On-Insulator technology.
    /// If set to `false` then the cells are designed for bulk CMOS.
    is_soi: Option<bool>,

    power_model: PowerModel,

    cells: BTreeMap<String, CellGroup>,
}

impl Default for Library {
    fn default() -> Self {
        Self {
            name: "".to_string(),
            technology: "".to_string(),
            delay_model: DelayModel::TableLookup,
            bus_naming_style: "".to_string(),
            revision: Default::default(),
            comment: Default::default(),
            date: Default::default(),
            units: Default::default(),

            trip_points: Default::default(),
            nom_process: 0.0,
            nom_voltage: 0.0,
            nom_temperature: 0.0,
            is_soi: None,
            power_model: PowerModel::TableLookup,
            cells: Default::default(),
        }
    }
}

#[derive(Debug, Clone, Default)]
pub struct CellGroup {
    name: String,
}

/// An item of a liberty group.
#[derive(Debug, Clone)]
enum GroupItem {
    Group(Group),
    Attribute { name: String, values: Vec<Value> },
    Define(Define),
}

/// Read a liberty library from a byte stream
pub fn read_liberty_bytes<R: Read>(reader: &mut R) -> Result<Library, LibertyParserError> {
    read_liberty_chars(reader.bytes().map(|b| b.unwrap() as char))
}

/// Read a liberty library from an iterator over characters.
pub fn read_liberty_chars<I>(chars: I) -> Result<Library, LibertyParserError>
where
    I: Iterator<Item = char>,
{
    let mut line_num = 0;
    let mut char_num = 0; // Position on the line.

    // Count newlines.
    let line_count = chars.inspect(|&c| {
        char_num += 1;
        if c == '\n' {
            line_num += 1;
            char_num = 0;
        }
    });

    let result = read_liberty_impl(line_count);

    if result.is_err() {
        log::error!("Liberty error on line: {} (at {})", line_num, char_num);
    }

    result
}

fn read_liberty_impl<I>(chars: I) -> Result<Library, LibertyParserError>
where
    I: Iterator<Item = char>,
{
    // Token stream.
    let mut tk = tokenize(chars, LibertyLexer::default());
    tk.advance();

    read_library(&mut tk)
}

/// Read a value like `(value)`, `value` with optionally appended `;`.
fn read_single_value_attr<V: FromStr, I: Iterator<Item = char> + PeekingNext>(
    tk: &mut Tokenized<I>,
) -> Result<V, LibertyParserError> {
    let parent = tk.test_str("(")?;
    let v: V = tk.take_and_parse()?;

    if parent {
        tk.expect_str(")")?;
    }
    tk.test_str(";")?;

    Ok(v)
}

/// Read a simple attribute value including the `:`.
fn parse_simple_value<V: FromStr, I: Iterator<Item = char> + PeekingNext>(
    tk: &mut Tokenized<I>,
) -> Result<V, LibertyParserError> {
    tk.expect_str(":")?;
    let v: V = tk.take_and_parse()?;
    tk.test_str(";")?;
    Ok(v)
}

/// Read a simple attribute value including the `:`.
fn read_simple_value<I: Iterator<Item = char> + PeekingNext>(
    tk: &mut Tokenized<I>,
) -> Result<Value, LibertyParserError> {
    tk.expect_str(":")?;
    let v = read_value(tk)?;
    tk.test_str(";")?;
    Ok(v)
}

/// Read a cell group without the preceeding `cell` token.
fn read_cell<I: Iterator<Item = char> + PeekingNext>(
    tk: &mut Tokenized<I>,
) -> Result<CellGroup, LibertyParserError> {
    let mut cell = CellGroup::default();

    tk.expect_str("(")?;
    cell.name = tk.take_str()?;
    tk.expect_str(")")?;

    tk.expect_str("{")?;

    // Read cell content.
    while !tk.test_str("}")? {}

    Ok(cell)
}

fn read_library<I: Iterator<Item = char> + PeekingNext>(
    tk: &mut Tokenized<I>,
) -> Result<Library, LibertyParserError> {
    let mut library = Library::default();

    tk.expect_str("library")?;
    tk.expect_str("(")?;
    library.name = tk.take_str()?;
    tk.expect_str(")")?;
    tk.expect_str("{")?;

    // Read library content.
    while !tk.test_str("}")? {
        let group_or_attr_name = tk.take_str()?;

        match group_or_attr_name.as_str() {
            "technology" => library.technology = read_single_value_attr(tk)?,
            "delay_model" => library.delay_model = parse_simple_value(tk)?,
            "bus_naming_style" => library.bus_naming_style = parse_simple_value(tk)?,

            // Trip-points.
            "slew_lower_threshold_pct_fall" => {
                library.trip_points.slew_lower_threshold_pct_fall = parse_simple_value(tk)?
            }
            "slew_upper_threshold_pct_fall" => {
                library.trip_points.slew_upper_threshold_pct_fall = parse_simple_value(tk)?
            }
            "slew_lower_threshold_pct_rise" => {
                library.trip_points.slew_lower_threshold_pct_rise = parse_simple_value(tk)?
            }
            "slew_upper_threshold_pct_rise" => {
                library.trip_points.slew_upper_threshold_pct_rise = parse_simple_value(tk)?
            }

            "input_threshold_pct_fall" => {
                library.trip_points.input_threshold_pct_fall = parse_simple_value(tk)?
            }
            "output_threshold_pct_fall" => {
                library.trip_points.output_threshold_pct_fall = parse_simple_value(tk)?
            }
            "input_threshold_pct_rise" => {
                library.trip_points.input_threshold_pct_rise = parse_simple_value(tk)?
            }
            "output_threshold_pct_rise" => {
                library.trip_points.output_threshold_pct_rise = parse_simple_value(tk)?
            }

            "slew_derate_from_library" => {
                library.trip_points.slew_derate_from_library = parse_simple_value(tk)?
            }

            "time_unit" => library.units.time_unit = parse_simple_value(tk)?,
            "voltage_unit" => unimplemented!(),
            "current_unit" => unimplemented!(),
            "pulling_resistance_unit" => unimplemented!(),
            "capacitive_load_unit" => unimplemented!(),
            "leakage_power_unit" => unimplemented!(),
            "distance_unit" => unimplemented!(),

            "comment" => library.comment = Some(read_simple_value(tk)?),
            "date" => library.date = Some(read_simple_value(tk)?),
            "revision" => library.revision = Some(read_simple_value(tk)?),

            "default_threshold_voltage_group" => unimplemented!(),
            "dist_conversion_factor" => unimplemented!(),
            "is_soi" => library.is_soi = Some(parse_simple_value(tk)?),

            "nom_process" => library.nom_process = parse_simple_value(tk)?,
            "nom_temperature" => library.nom_temperature = parse_simple_value(tk)?,
            "nom_voltage" => library.nom_voltage = parse_simple_value(tk)?,

            "power_model" => library.power_model = parse_simple_value(tk)?,

            "cell" => {
                let cell = read_cell(tk)?;
                let old_entry = library.cells.insert(cell.name.clone(), cell);
                if let Some(old) = old_entry {
                    log::warn!(
                        "Cell group `{}` already exists and is overwritten.",
                        old.name
                    );
                }
            }
            _ => return Err(LibertyParserError::UnknownToken(group_or_attr_name)),
        }
    }

    Ok(library)
}

// /// Read a name with an optional bit selection.
// /// Something like `name[15:0]`.
// fn read_name<I: Iterator<Item=char> + PeekingNext>(tk: &mut Tokenized<I>) -> Result<String, ParserError>
// {
//     let name = tk.take_str()?;
//     if tk.test_str("[")? {
//         let end: u32 = tk.take_and_parse()?;
//         if tk.test_str(":")? {
//             let start: u32 = tk.take_and_parse()?;
//         }
//         tk.expect_str("]")?;
//     }
//
//     Ok(name)
// }

/// Read a value (string, float, int, ...).
fn read_value<I: Iterator<Item = char> + PeekingNext>(
    tk: &mut Tokenized<I>,
) -> Result<Value, LibertyParserError> {
    let s = tk
        .current_token_str()
        .ok_or(LibertyParserError::UnexpectedEndOfFile)?;
    let first_char = s
        .chars()
        .next()
        .ok_or(LibertyParserError::UnexpectedEndOfFile)?;
    let last_char = s
        .chars()
        .last()
        .ok_or(LibertyParserError::UnexpectedEndOfFile)?;
    if first_char.is_ascii_digit() && last_char.is_ascii_digit() {
        // Read a number.
        let f: f64 = tk.take_and_parse()?;
        Ok(Value::Float(f))
    } else if first_char == '"' {
        // Strip away the quotes.
        let without_quotes = String::from_iter(s.chars().skip(1).take(s.len() - 2));
        tk.advance();
        Ok(Value::QuotedString(without_quotes))
    } else {
        let name = tk.take_str()?;

        if tk.test_str("[")? {
            let end: u32 = tk.take_and_parse()?;
            let start = if tk.test_str(":")? {
                let start: u32 = tk.take_and_parse()?;
                Some(start)
            } else {
                None
            };
            tk.expect_str("]")?;
            if let Some(start) = start {
                Ok(Value::Sliced(name, end, start))
            } else {
                Ok(Value::Indexed(name, end))
            }
        } else {
            Ok(Value::String(name))
        }
    }
}

/// Read a liberty group.
fn read_group_item<I: Iterator<Item = char> + PeekingNext>(
    tk: &mut Tokenized<I>,
) -> Result<GroupItem, LibertyParserError> {
    let name = tk.take_str()?;
    if tk.test_str("(")? {
        // Group or complex attribute.
        let mut args = vec![];
        while !tk.test_str(")")? {
            args.push(read_value(tk)?);
            if !tk.peeking_test_str(")")? {
                tk.expect_str(",")?;
            }
        }

        if tk.test_str("{")? {
            // It's a group.

            let mut group = Group {
                name,
                arguments: args,
                ..Default::default()
            };

            while !tk.test_str("}")? {
                // Recursively read group items.
                let item = read_group_item(tk)?;
                match item {
                    GroupItem::Group(g) => group.groups.push(g),
                    GroupItem::Attribute { name, values } => {
                        group.attributes.entry(name).or_default().push(values);
                    }
                    GroupItem::Define(d) => group.defines.push(d),
                }
            }
            Ok(GroupItem::Group(group))
        } else {
            // It's a complex attribute or define statement.
            tk.test_str(";")?; // Consume an optional trailing semicolon.
            if name == "define" && args.len() == 3 {
                // Define statement.

                // Values must be names or quoted names:
                let attribute_name = match &args[0] {
                    Value::String(s) => s.clone(),
                    Value::QuotedString(s) => s.clone(),
                    a => a.to_string(),
                };
                let group_name = match &args[1] {
                    Value::String(s) => s.clone(),
                    Value::QuotedString(s) => s.clone(),
                    a => a.to_string(),
                };
                let attr_type = match &args[2] {
                    Value::String(s) => s.clone(),
                    Value::QuotedString(s) => s.clone(),
                    a => a.to_string(),
                };

                // Parse the attribute type.
                let attribute_type = AttributeType::from_str(attr_type.as_str())
                    .map_err(|_| LibertyParserError::UnexpectedToken("".to_string(), attr_type))?;

                Ok(GroupItem::Define(Define {
                    attribute_name,
                    group_name,
                    attribute_type,
                }))
            } else {
                // It's a complex attribute.
                Ok(GroupItem::Attribute { name, values: args })
            }
        }
    } else if tk.test_str(":")? {
        // Simple attribute.
        let value = read_value(tk)?;
        tk.expect_str(";")?;

        Ok(GroupItem::Attribute {
            name,
            values: vec![value],
        })
    } else {
        Err(LibertyParserError::UnexpectedToken(
            "'(' | ':'".into(),
            tk.current_token_str().unwrap(),
        ))
    }
}

#[test]
fn test_read_liberty() {
    //     let data = r#"
    // library (myLib) {
    //     simple_attribute1: value;
    //     simple_attribute2: value;
    //     complex_attribute1 (value1, "value 2");
    //
    //     // Single line comment // does not end here / also not here
    //
    //     /* comment with ** * characters */
    //
    //     cell (invx1) {
    //         simple_attribute1: value;
    //     }
    // }
    // "#;
    let data = r#"
library (myLib) {
    // Some comment.
    technology (cmos);
    delay_model : table_lookup;
    bus_naming_style : "%s_%d";

    slew_lower_threshold_pct_fall : 50.0;

    time_unit : 1ns;

    is_soi : false;

    cell(INVX1) {
    }

    cell(INVX2) {
    }
}
"#;

    let result = read_liberty_chars(data.chars());

    dbg!(&result);

    assert!(result.is_ok());

    let lib = result.unwrap();
    assert_eq!(lib.name, "myLib");
    assert_eq!(lib.delay_model, DelayModel::TableLookup);
}

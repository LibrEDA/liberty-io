// Copyright (c) 2021-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Functions for parsing ASCII-based formats from iterators over bytes.

use itertools::{Itertools, PeekingNext};
use libreda_stream_parser::{Lexer, ParserError};
use std::fmt;
use std::iter::Peekable;
use std::num::ParseIntError;
use std::str::FromStr;

/// Error while parsing Liberty.
/// TODO: Separate lexer errors from Liberty specific errors.
#[derive(Clone, Debug)]
pub enum LibertyParserError {
    /// Error during parsing.
    ParserError(ParserError<char>),
    InvalidCharacter,
    UnexpectedEndOfFile,
    /// Expected and actual token.
    UnexpectedToken(String, String),
    UnknownToken(String),
    InvalidLiteral(String),
    NotImplemented(&'static str),
    ParseIntError(ParseIntError),
    Other(&'static str),
}

impl fmt::Display for LibertyParserError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            LibertyParserError::InvalidCharacter => write!(f, "Invalid character."),
            LibertyParserError::UnexpectedEndOfFile => write!(f, "Unexpected end of file."),
            LibertyParserError::UnexpectedToken(actual, exp) => {
                write!(f, "Unexpected token. '{}' instead of '{}'", actual, exp)
            }
            LibertyParserError::UnknownToken(t) => write!(f, "Unknown token: '{}'.", t),
            LibertyParserError::InvalidLiteral(n) => write!(f, "Invalid literal: '{}'.", n),
            LibertyParserError::NotImplemented(n) => write!(f, "Not implemented: '{}'.", n),
            LibertyParserError::Other(msg) => write!(f, "'{}'.", msg),
            LibertyParserError::ParseIntError(e) => write!(f, "Illegal integer: '{}'", e),
            LibertyParserError::ParserError(e) => write!(f, "{}", e),
        }
    }
}

impl From<ParserError<char>> for LibertyParserError {
    fn from(e: ParserError<char>) -> Self {
        Self::ParserError(e)
    }
}

impl From<ParseIntError> for LibertyParserError {
    fn from(e: ParseIntError) -> Self {
        Self::ParseIntError(e)
    }
}

fn is_terminal_char(c: char) -> bool {
    matches!(c, '{' | '}' | '(' | ')' | '[' | ']' | ';' | ',' | '/' | '*')
}

#[derive(Default, Clone)]
pub struct LibertyLexer {}

impl Lexer for LibertyLexer {
    type Char = char;
    fn consume_next_token(
        &mut self,
        iter: &mut (impl Iterator<Item = Self::Char> + PeekingNext),
        mut output: impl FnMut(Self::Char),
    ) -> Result<(), ParserError<char>> {
        loop {
            // Skip whitespace but not newlines.
            let _n = iter.peeking_take_while(|c| c.is_whitespace()).count();

            // Look ahead.
            if let Some(c) = iter.peeking_next(|_| true) {
                debug_assert!(!c.is_whitespace() || c == '\n');

                match c {
                    '#' => {
                        // Skip comments.
                        iter.peeking_take_while(|&c| c != '\n' && c != '\r').count();
                    }
                    '/' => {
                        if iter.peeking_next(|&c| c == '*').is_some() {
                            skip_multiline_comment(iter);
                        } else if iter.peeking_next(|&c| c == '/').is_some() {
                            // Skip comment.

                            // Consume until next '\n'.
                            iter.peeking_take_while(|&c| c != '\n').count();
                        } else {
                            output(c);
                        }
                    }
                    '\\' => {
                        if iter.peeking_next(|&c| c == '\\').is_some() {
                            // Output escape character.
                            output('\\');
                        } else if iter.peeking_next(|&c| c == '\n').is_some() {
                            // Ignore masked newlines.
                            iter.next();
                        } else if iter.peeking_next(|&c| c == '\r').is_some() {
                            // Ignore masked newlines.
                            iter.next();
                            if iter.peeking_next(|&c| c == '\n').is_some() {
                                iter.next();
                            }
                        } else {
                            output('\\');
                        }
                    }
                    '"' | '\'' => {
                        // Quoted string.
                        let quote_char = c;
                        return read_quoted_string(quote_char, &mut output, iter);
                    }
                    _ => {
                        // Normal token.
                        let first_char = c;
                        return read_normal_token(first_char, &mut output, iter);
                    }
                }
            } else {
                return Ok(());
            }
        }
        Ok(())
    }
}

fn read_normal_token(
    first_char: char,
    output: &mut impl FnMut(<LibertyLexer as Lexer>::Char),
    iter: &mut (impl Iterator<Item = <LibertyLexer as Lexer>::Char> + PeekingNext),
) -> Result<(), ParserError<char>> {
    let mut prev = first_char;
    output(first_char);
    if is_terminal_char(first_char) {
        return Ok(());
    }
    while let Some(c) = iter.peeking_next(|&c| !is_terminal_char(c)) {
        if prev != '\\' && (c.is_whitespace() || is_terminal_char(c)) {
            // Abort on unmasked whitespace or terminal character.
            break;
        }

        output(c);
        prev = c;
    }

    Ok(())
}

/// Read a quoted string.
/// Assumes the first quote character to be consumed already.
/// Output includes the beginning and ending quote character.
fn read_quoted_string(
    quote_char: char,
    output: &mut impl FnMut(<LibertyLexer as Lexer>::Char),
    iter: &mut (impl Iterator<Item = <LibertyLexer as Lexer>::Char> + PeekingNext),
) -> Result<(), ParserError<char>> {
    output(quote_char);
    let mut prev = None;
    while let Some(c) = iter.next() {
        if prev != Some('\\') {
            output(c);
            if c == quote_char {
                // Abort on quote char.
                break;
            }
        } else {
            // '\'...
            if c == '\n' {
                // Skip quoted newline.
            } else if c == '\r' {
                // Consume a following \n, if any.
                iter.peeking_next(|&c| c == '\n');
            } else {
                output(c);
            }
        }
        prev = Some(c);
    }
    Ok(())
}

fn skip_multiline_comment<I>(iter: &mut I)
where
    I: Iterator<Item = char> + PeekingNext,
{
    loop {
        // Consume until next '*'.
        iter.peeking_take_while(|&c| c != '*').count();
        if iter.next() != Some('*') {
            break; // End of file.
        }

        if iter.peeking_next(|&c| c == '/').is_some() {
            // End of comment.
            break;
        }
    }
}

/// Read simple tokens and skip comments.
#[test]
fn test_read_token() {
    let data = r#"
        # Comment 1

        # Comment 2

        /* multi
        line
        comment */

        token1

        # Comment 3

        token2(token3, "quoted token") {token4}

        /**/

        token5 : token6;

    # Masked newline should be ignored.
    \
    token7
    "#;

    let mut iter = data.chars().inspect(|c| print!("{}", c)).peekable();

    let mut buffer = String::new();

    assert_eq!(read_token(&mut iter, &mut buffer), Some("token1"));
    assert_eq!(read_token(&mut iter, &mut buffer), Some("token2"));
    assert_eq!(read_token(&mut iter, &mut buffer), Some("("));
    assert_eq!(read_token(&mut iter, &mut buffer), Some("token3"));
    assert_eq!(read_token(&mut iter, &mut buffer), Some(","));
    assert_eq!(read_token(&mut iter, &mut buffer), Some("\"quoted token\""));
    assert_eq!(read_token(&mut iter, &mut buffer), Some(")"));
    assert_eq!(read_token(&mut iter, &mut buffer), Some("{"));
    assert_eq!(read_token(&mut iter, &mut buffer), Some("token4"));
    assert_eq!(read_token(&mut iter, &mut buffer), Some("}"));
    assert_eq!(read_token(&mut iter, &mut buffer), Some("token5"));
    assert_eq!(read_token(&mut iter, &mut buffer), Some(":"));
    assert_eq!(read_token(&mut iter, &mut buffer), Some("token6"));
    assert_eq!(read_token(&mut iter, &mut buffer), Some(";"));
    assert_eq!(read_token(&mut iter, &mut buffer), Some("token7"));

    /// Read a token into the buffer. Tokens are separated by white space. Comments are ignored.
    /// Quoted tokens can contain white space.
    /// Used for testing only.
    fn read_token<'a, I>(iter: &mut I, buffer: &'a mut String) -> Option<&'a str>
    where
        I: Iterator<Item = char> + PeekingNext,
    {
        buffer.clear();

        let iter = iter.by_ref();

        LibertyLexer::default()
            .consume_next_token(iter, |c| buffer.push(c))
            .ok()
            .map(|_| buffer.as_str())
    }
}

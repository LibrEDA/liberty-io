// Copyright (c) 2021-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::ast::*;
use crate::stream_lexer::*;
use itertools::PeekingNext;
use libreda_stream_parser::*;
use std::io::Read;
use std::iter::FromIterator;
use std::str::FromStr;

/// An item of a liberty group.
#[derive(Debug, Clone)]
enum GroupItem {
    Group(Group),
    Attribute { name: String, values: Vec<Value> },
    Define(Define),
}

/// Read a liberty library from a byte stream
pub fn read_liberty_bytes<R: Read>(reader: &mut R) -> Result<Group, LibertyParserError> {
    read_liberty_chars(reader.bytes().map(|b| b.unwrap() as char))
}

/// Read a liberty library from an iterator over characters.
pub fn read_liberty_chars<I>(chars: I) -> Result<Group, LibertyParserError>
where
    I: Iterator<Item = char>,
{
    let mut line_num = 0;
    let mut char_num = 0; // Position on the line.

    // Count newlines.
    let line_count = chars.inspect(|&c| {
        char_num += 1;
        if c == '\n' {
            line_num += 1;
            char_num = 0;
        }
    });

    let result = read_liberty_impl(line_count);

    if result.is_err() {
        log::error!("Liberty error on line: {} (at {})", line_num, char_num);
    }

    result
}

fn read_liberty_impl<I>(chars: I) -> Result<Group, LibertyParserError>
where
    I: Iterator<Item = char>,
{
    // Token stream.
    let mut tk = tokenize(chars, LibertyLexer::default());
    tk.advance();

    let item = read_group_item(&mut tk)?;
    match item {
        GroupItem::Group(g) => Ok(g),
        _ => Err(LibertyParserError::Other(
            "Library must start with a group.",
        )),
    }
}

fn read_value<I: Iterator<Item = char> + PeekingNext>(
    tk: &mut Tokenized<I, LibertyLexer>,
) -> Result<Value, LibertyParserError> {
    let s = tk
        .current_token_str()
        .ok_or(LibertyParserError::UnexpectedEndOfFile)?;
    let first_char = s
        .chars()
        .next()
        .ok_or(LibertyParserError::UnexpectedEndOfFile)?;
    let last_char = s
        .chars()
        .last()
        .ok_or(LibertyParserError::UnexpectedEndOfFile)?;
    if first_char.is_ascii_digit() && last_char.is_ascii_digit() {
        // Read a number.

        if s.chars()
            .enumerate()
            // An integer can start with a '-' but then consists only of digits.
            .all(|(idx, c)| c.is_ascii_digit() || (idx == 0 && c == '-'))
        {
            // Read integer.
            Ok(Value::Integer(tk.take_and_parse()?))
        } else {
            // Read floating point number.
            Ok(Value::Float(tk.take_and_parse()?))
        }
    } else if first_char == '"' {
        // Strip away the quotes.
        let without_quotes = String::from_iter(s.chars().skip(1).take(s.len() - 2));
        tk.advance();
        Ok(Value::QuotedString(without_quotes))
    } else {
        let name = tk.take_str()?;

        if tk.test_str("[")? {
            let buspins = tk.take_str()?; // TODO: Taking ownership is not necessary here.
            let mut splitted = buspins.splitn(2, ':');

            let end: u32 = splitted.next().unwrap().parse()?;

            let start_str: Option<_> = splitted.next();
            tk.expect_str("]")?;
            if let Some(start_str) = start_str {
                let start = start_str.parse()?;
                Ok(Value::Sliced(name, end, start))
            } else {
                Ok(Value::Indexed(name, end))
            }
        } else {
            Ok(Value::String(name))
        }
    }
}

/// Read a liberty group.
fn read_group_item<I: Iterator<Item = char> + PeekingNext>(
    tk: &mut Tokenized<I, LibertyLexer>,
) -> Result<GroupItem, LibertyParserError> {
    let name = tk.take_str()?;
    if tk.test_str("(")? {
        // Group or complex attribute.
        let mut args = vec![];
        while !tk.test_str(")")? {
            args.push(read_value(tk)?);
            if !tk.peeking_test_str(")")? {
                tk.expect_str(",")?;
            }
        }

        if tk.test_str("{")? {
            // It's a group.

            let mut group = Group {
                name,
                arguments: args,
                ..Default::default()
            };

            while !tk.test_str("}")? {
                // Recursively read group items.
                let item = read_group_item(tk)?;
                match item {
                    GroupItem::Group(g) => group.groups.push(g),
                    GroupItem::Attribute { name, values } => {
                        group.attributes.entry(name).or_default().push(values);
                    }
                    GroupItem::Define(d) => group.defines.push(d),
                }
            }
            Ok(GroupItem::Group(group))
        } else {
            // It's a complex attribute or define statement.
            tk.test_str(";")?; // Consume an optional trailing semicolon.
            if name == "define" && args.len() == 3 {
                // Define statement.

                // Values must be names or quoted names:
                let attribute_name = match &args[0] {
                    Value::String(s) => s.clone(),
                    Value::QuotedString(s) => s.clone(),
                    a => a.to_string(),
                };
                let group_name = match &args[1] {
                    Value::String(s) => s.clone(),
                    Value::QuotedString(s) => s.clone(),
                    a => a.to_string(),
                };
                let attr_type = match &args[2] {
                    Value::String(s) => s.clone(),
                    Value::QuotedString(s) => s.clone(),
                    a => a.to_string(),
                };

                // Parse the attribute type.
                let attribute_type = AttributeType::from_str(attr_type.as_str())
                    .map_err(|_| LibertyParserError::UnexpectedToken("".to_string(), attr_type))?;

                Ok(GroupItem::Define(Define {
                    attribute_name,
                    group_name,
                    attribute_type,
                }))
            } else {
                // It's a complex attribute.
                Ok(GroupItem::Attribute { name, values: args })
            }
        }
    } else if name.ends_with(":") || tk.test_str(":")? {
        // Simple attribute.
        let value = read_value(tk)?;

        // Fix for supporting absent whitespace between attribute name and colon.
        let mut name = name;
        if name.ends_with(":") {
            name.truncate(name.len() - 1);
        }

        tk.expect_str(";")?;

        Ok(GroupItem::Attribute {
            name,
            values: vec![value],
        })
    } else {
        Err(LibertyParserError::UnexpectedToken(
            "'(' | ':'".into(),
            tk.current_token_str().unwrap(),
        ))
    }
}

#[test]
fn test_tokenized() {
    let data = r#"
        # Comment 1

        # Comment 2

        /* multi
        line
        comment */

        token1

        # Comment 3

        token2(token3, "quoted token") {token4}

        /**/

        token5:token6;

    "#;

    let mut tokens = tokenize(data.chars(), LibertyLexer::default());
    tokens.advance();

    assert!(tokens.expect_str("token1").is_ok());
    assert!(tokens.expect_str("token2").is_ok());
    assert!(tokens.expect_str("(").is_ok());
    assert!(tokens.expect_str("token3").is_ok());
    assert!(tokens.expect_str(",").is_ok());
    assert!(tokens.expect_str("\"quoted token\"").is_ok());
    assert!(tokens.expect_str(")").is_ok());
    assert!(tokens.expect_str("{").is_ok());
    assert!(tokens.expect_str("token4").is_ok());
}

#[test]
fn test_read_liberty() {
    let data = r#"
library (myLib) {
    simple_attribute1 : value;
    simple_attribute2 : value;
    complex_attribute1 (value1, "value 2");

    // Single line comment // does not end here / also not here

    /* comment with ** * characters */

    cell (invx1) {
        simple_attribute1 : value;
    }
}
"#;

    let result = read_liberty_chars(data.chars());

    dbg!(&result);

    assert!(result.is_ok());
}

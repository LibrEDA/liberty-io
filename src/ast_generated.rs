// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Enum-like types.

use std::fmt;

/* Generate liberty enums:

```python
def gen_lib_enum(name, values, doc=""):

    def snake2camel(s):
        return "".join((
            n.capitalize() for n in s.split("_")
        ))

    match_statements = [f'"{v}" => Ok(Self::{snake2camel(v)})' for v in values]

    format_statements = [f'Self::{snake2camel(v)} => f.write_str("{v}")' for v in values]

    doc = doc.replace("\n", "\n /// ")

    return f"""
/// {doc}
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum {name} {{
    {", ".join((snake2camel(v) for v in values))}
}}

impl FromStr for {name} {{
    type Err = ();

    fn from_str(input: &str) -> Result<Self, Self::Err> {{
        match input {{
            {", ".join(match_statements)},
            _ => Err(()),
        }}
    }}
}}

impl fmt::Display for {name} {{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {{
        match self {{
            {", ".join(format_statements)}
        }}
    }}
}}

"""

print(gen_lib_enum("DelayModel", ["generic_cmos", "table_lookup", "piecewise_cmos", "dcm", "polynomial"]))
print(gen_lib_enum("PowerModel", ["generic_cmos", "table_lookup", "piecewise_cmos", "dcm", "polynomial"]))
print(gen_lib_enum("TimingType", ["combinational", "combinational_rise", "combinational_fall",
    "three_state_disable", "three_state_enable", "three_state_disable_rise", "three_state_disable_fall",
    "three_state_enable_rise", "three_state_enable_fall",
    "rising_edge", "falling_edge", "preset", "clear", "hold_rising", "hold_falling", "setup_rising", "setup_falling",
    "recovery_rising", "recovery_falling", "skew_rising", "skew_falling", "removal_rising", "removal_falling",
    "min_pulse_width", "minimum_period", "max_clock_tree_path", "min_clock_tree_path",
    "non_seq_setup_rising", "non_seq_setup_falling", "non_seq_hold_rising", "non_seq_hold_falling",
    "nochange_high_high", "nochange_high_low", "nochange_low_high", "nochange_low_low",
    ]))


# print(gen_lib_enum("TimeUnit", ["1ps", "10ps", "100ps", "1ns"]))

```
*/

use std::fmt::Write;
use std::str::FromStr;

///
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum DelayModel {
    GenericCmos,
    TableLookup,
    PiecewiseCmos,
    Dcm,
    Polynomial,
}

impl FromStr for DelayModel {
    type Err = ();

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        match input {
            "generic_cmos" => Ok(Self::GenericCmos),
            "table_lookup" => Ok(Self::TableLookup),
            "piecewise_cmos" => Ok(Self::PiecewiseCmos),
            "dcm" => Ok(Self::Dcm),
            "polynomial" => Ok(Self::Polynomial),
            _ => Err(()),
        }
    }
}

impl fmt::Display for DelayModel {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::GenericCmos => f.write_str("generic_cmos"),
            Self::TableLookup => f.write_str("table_lookup"),
            Self::PiecewiseCmos => f.write_str("piecewise_cmos"),
            Self::Dcm => f.write_str("dcm"),
            Self::Polynomial => f.write_str("polynomial"),
        }
    }
}

///
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum PowerModel {
    GenericCmos,
    TableLookup,
    PiecewiseCmos,
    Dcm,
    Polynomial,
}

impl FromStr for PowerModel {
    type Err = ();

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        match input {
            "generic_cmos" => Ok(Self::GenericCmos),
            "table_lookup" => Ok(Self::TableLookup),
            "piecewise_cmos" => Ok(Self::PiecewiseCmos),
            "dcm" => Ok(Self::Dcm),
            "polynomial" => Ok(Self::Polynomial),
            _ => Err(()),
        }
    }
}

impl fmt::Display for PowerModel {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::GenericCmos => f.write_str("generic_cmos"),
            Self::TableLookup => f.write_str("table_lookup"),
            Self::PiecewiseCmos => f.write_str("piecewise_cmos"),
            Self::Dcm => f.write_str("dcm"),
            Self::Polynomial => f.write_str("polynomial"),
        }
    }
}

///
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum TimingType {
    Combinational,
    CombinationalRise,
    CombinationalFall,
    ThreeStateDisable,
    ThreeStateEnable,
    ThreeStateDisableRise,
    ThreeStateDisableFall,
    ThreeStateEnableRise,
    ThreeStateEnableFall,
    RisingEdge,
    FallingEdge,
    Preset,
    Clear,
    HoldRising,
    HoldFalling,
    SetupRising,
    SetupFalling,
    RecoveryRising,
    RecoveryFalling,
    SkewRising,
    SkewFalling,
    RemovalRising,
    RemovalFalling,
    MinPulseWidth,
    MinimumPeriod,
    MaxClockTreePath,
    MinClockTreePath,
    NonSeqSetupRising,
    NonSeqSetupFalling,
    NonSeqHoldRising,
    NonSeqHoldFalling,
    NochangeHighHigh,
    NochangeHighLow,
    NochangeLowHigh,
    NochangeLowLow,
}

impl FromStr for TimingType {
    type Err = ();

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        match input {
            "combinational" => Ok(Self::Combinational),
            "combinational_rise" => Ok(Self::CombinationalRise),
            "combinational_fall" => Ok(Self::CombinationalFall),
            "three_state_disable" => Ok(Self::ThreeStateDisable),
            "three_state_enable" => Ok(Self::ThreeStateEnable),
            "three_state_disable_rise" => Ok(Self::ThreeStateDisableRise),
            "three_state_disable_fall" => Ok(Self::ThreeStateDisableFall),
            "three_state_enable_rise" => Ok(Self::ThreeStateEnableRise),
            "three_state_enable_fall" => Ok(Self::ThreeStateEnableFall),
            "rising_edge" => Ok(Self::RisingEdge),
            "falling_edge" => Ok(Self::FallingEdge),
            "preset" => Ok(Self::Preset),
            "clear" => Ok(Self::Clear),
            "hold_rising" => Ok(Self::HoldRising),
            "hold_falling" => Ok(Self::HoldFalling),
            "setup_rising" => Ok(Self::SetupRising),
            "setup_falling" => Ok(Self::SetupFalling),
            "recovery_rising" => Ok(Self::RecoveryRising),
            "recovery_falling" => Ok(Self::RecoveryFalling),
            "skew_rising" => Ok(Self::SkewRising),
            "skew_falling" => Ok(Self::SkewFalling),
            "removal_rising" => Ok(Self::RemovalRising),
            "removal_falling" => Ok(Self::RemovalFalling),
            "min_pulse_width" => Ok(Self::MinPulseWidth),
            "minimum_period" => Ok(Self::MinimumPeriod),
            "max_clock_tree_path" => Ok(Self::MaxClockTreePath),
            "min_clock_tree_path" => Ok(Self::MinClockTreePath),
            "non_seq_setup_rising" => Ok(Self::NonSeqSetupRising),
            "non_seq_setup_falling" => Ok(Self::NonSeqSetupFalling),
            "non_seq_hold_rising" => Ok(Self::NonSeqHoldRising),
            "non_seq_hold_falling" => Ok(Self::NonSeqHoldFalling),
            "nochange_high_high" => Ok(Self::NochangeHighHigh),
            "nochange_high_low" => Ok(Self::NochangeHighLow),
            "nochange_low_high" => Ok(Self::NochangeLowHigh),
            "nochange_low_low" => Ok(Self::NochangeLowLow),
            _ => Err(()),
        }
    }
}

impl fmt::Display for TimingType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Combinational => f.write_str("combinational"),
            Self::CombinationalRise => f.write_str("combinational_rise"),
            Self::CombinationalFall => f.write_str("combinational_fall"),
            Self::ThreeStateDisable => f.write_str("three_state_disable"),
            Self::ThreeStateEnable => f.write_str("three_state_enable"),
            Self::ThreeStateDisableRise => f.write_str("three_state_disable_rise"),
            Self::ThreeStateDisableFall => f.write_str("three_state_disable_fall"),
            Self::ThreeStateEnableRise => f.write_str("three_state_enable_rise"),
            Self::ThreeStateEnableFall => f.write_str("three_state_enable_fall"),
            Self::RisingEdge => f.write_str("rising_edge"),
            Self::FallingEdge => f.write_str("falling_edge"),
            Self::Preset => f.write_str("preset"),
            Self::Clear => f.write_str("clear"),
            Self::HoldRising => f.write_str("hold_rising"),
            Self::HoldFalling => f.write_str("hold_falling"),
            Self::SetupRising => f.write_str("setup_rising"),
            Self::SetupFalling => f.write_str("setup_falling"),
            Self::RecoveryRising => f.write_str("recovery_rising"),
            Self::RecoveryFalling => f.write_str("recovery_falling"),
            Self::SkewRising => f.write_str("skew_rising"),
            Self::SkewFalling => f.write_str("skew_falling"),
            Self::RemovalRising => f.write_str("removal_rising"),
            Self::RemovalFalling => f.write_str("removal_falling"),
            Self::MinPulseWidth => f.write_str("min_pulse_width"),
            Self::MinimumPeriod => f.write_str("minimum_period"),
            Self::MaxClockTreePath => f.write_str("max_clock_tree_path"),
            Self::MinClockTreePath => f.write_str("min_clock_tree_path"),
            Self::NonSeqSetupRising => f.write_str("non_seq_setup_rising"),
            Self::NonSeqSetupFalling => f.write_str("non_seq_setup_falling"),
            Self::NonSeqHoldRising => f.write_str("non_seq_hold_rising"),
            Self::NonSeqHoldFalling => f.write_str("non_seq_hold_falling"),
            Self::NochangeHighHigh => f.write_str("nochange_high_high"),
            Self::NochangeHighLow => f.write_str("nochange_high_low"),
            Self::NochangeLowHigh => f.write_str("nochange_low_high"),
            Self::NochangeLowLow => f.write_str("nochange_low_low"),
        }
    }
}

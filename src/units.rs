// SPDX-FileCopyrightText: 2023 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Data types for representing physical units.

use std::{fmt, str::FromStr};

use crate::Value;

/// Unit of time.
#[derive(Copy, Clone, Debug)]
pub enum TimeUnit {
    /// 1ps.
    Ps1,
    /// 10ps.
    Ps10,
    /// 100ps.
    Ps100,
    /// 1ns.
    Ns1,
}

impl TimeUnit {
    /// Get the duration of one time unit in seconds.
    pub fn as_seconds(self) -> f64 {
        match self {
            TimeUnit::Ps1 => 1e-12,
            TimeUnit::Ps10 => 10e-12,
            TimeUnit::Ps100 => 100e-12,
            TimeUnit::Ns1 => 1e-9,
        }
    }
}

impl FromStr for TimeUnit {
    type Err = ();

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        match input {
            "1ps" => Ok(Self::Ps1),
            "10ps" => Ok(Self::Ps10),
            "100ps" => Ok(Self::Ps100),
            "1ns" => Ok(Self::Ns1),
            _ => Err(()),
        }
    }
}

impl fmt::Display for TimeUnit {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(match self {
            Self::Ps1 => "1ps",
            Self::Ps10 => "10ps",
            Self::Ps100 => "100ps",
            Self::Ns1 => "1ns",
        })
    }
}

#[test]
fn test_time_unit() {
    assert_eq!(TimeUnit::from_str("100ps").unwrap().as_seconds(), 100e-12);
}

/// Unit of capacitive loads.
#[derive(Copy, Clone, Debug)]
pub enum CapacitiveLoadUnit {
    /// 'pf'
    PicoFarad(f64),
    /// 'ff'
    FemtoFarad(f64),
}

impl CapacitiveLoadUnit {
    /// Convert to farads.
    pub fn as_farad(self) -> f64 {
        match self {
            CapacitiveLoadUnit::PicoFarad(c) => c * 1e-12,
            CapacitiveLoadUnit::FemtoFarad(c) => c * 1e-15,
        }
    }
}

impl TryFrom<&[Value]> for CapacitiveLoadUnit {
    type Error = ();

    fn try_from(cplx_attr: &[Value]) -> Result<Self, Self::Error> {
        if cplx_attr.len() != 2 {
            return Err(());
        }
        let num = cplx_attr[0].as_float().ok_or(())?;

        let subscript = cplx_attr[1].as_str().ok_or(())?;

        if subscript.len() != 2 {
            return Err(());
        }

        if !["f", "F"].contains(&&subscript[1..]) {
            return Err(());
        }

        match &subscript[0..1] {
            "f" => Ok(Self::FemtoFarad(num)),
            "p" => Ok(Self::PicoFarad(num)),
            _ => Err(()),
        }
    }
}

impl fmt::Display for CapacitiveLoadUnit {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            CapacitiveLoadUnit::PicoFarad(c) => write!(f, "{} pf", c),
            CapacitiveLoadUnit::FemtoFarad(c) => write!(f, "{} ff", c),
        }
    }
}

#[test]
fn test_parse_capacitive_load_unit() {
    let v = [Value::Float(1.25), Value::String("pf".into())];

    let cap = CapacitiveLoadUnit::try_from(v.as_slice()).unwrap();

    assert_eq!(cap.as_farad(), 1.25e-12);
}

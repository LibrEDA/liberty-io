// Copyright (c) 2021-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Utility functions for handling Liberty data.

use crate::boolean::BooleanExpr;
use crate::{Group, Value};
use std::num::ParseFloatError;
use std::str::FromStr;

impl Group {
    /// Find an attribute by name.
    pub fn get_attribute(&self, name: &str) -> Option<&Vec<Value>> {
        self.attributes
            .get(name)
            .and_then(|attributes| attributes.first())
    }

    /// Get the value of the first simple attribute with the given name.
    /// Complex attributes with the same name are ignored.
    pub fn get_simple_attribute(&self, name: &str) -> Option<&Value> {
        self.attributes
            .get(name)
            .and_then(|attributes| attributes.first())
            .filter(|values| values.len() == 1) // Take simple attributes only.
            .and_then(|values| values.first())
    }

    /// Get the value of the first simple attribute with the given name.
    /// Simple attributes with the same name are ignored.
    pub fn get_complex_attribute(&self, name: &str) -> Option<&Vec<Value>> {
        self.attributes
            .get(name)
            .and_then(|attributes| attributes.first())
            .filter(|values| values.len() != 1) // Take simple attributes only.
    }

    /// Parse an attribute value as a Boolean function.
    /// Returns `None` if the attribute cannot be parsed as a Boolean function.
    fn get_boolean_function(&self, name: &str) -> Option<(BooleanExpr<usize>, Vec<String>)> {
        self.get_simple_attribute(name).and_then(|attr| match attr {
            Value::String(s) | Value::QuotedString(s) => {
                crate::boolean::parse_boolean_function(s.chars().peekable()).ok()
            }
            _ => None,
        })
    }

    /// Iterate over sub-groups with a specific name.
    pub fn find_groups_by_name<'a: 'b, 'b>(
        &'a self,
        group_name: &'b str,
    ) -> impl Iterator<Item = &'a Group> + 'b {
        self.groups.iter().filter(move |g| g.name == group_name)
    }

    /// Select a group by the group name and its first argument.
    /// Takes the first one in case there's multiple matches.
    ///
    /// For example a `pin(A) {...}` group can be selected with
    /// `find_group_by_arg(group, "pin", "A")`.
    pub fn find_group_by_arg(&self, group_name: &str, arg_name: &str) -> Option<&Group> {
        self.find_groups_by_name(group_name).find(|g| {
            !g.arguments.is_empty()
                && if let Value::String(name) = &g.arguments[0] {
                    name == arg_name
                } else {
                    false
                }
        })
    }

    /// Select a cell by name from a library group.
    /// Takes the first one in case there's multiple matches.
    ///
    /// # Panics
    /// Panics if the group is not a library group.
    pub fn find_cell(&self, cell_name: &str) -> Option<&Group> {
        assert_eq!(self.name, "library", "Group must be the top-level library.");
        self.find_group_by_arg("cell", cell_name)
    }

    /// Select a pin by name from a cell group.
    /// Takes the first one in case there's multiple matches.
    ///
    /// # Panics
    /// Panics if the group is not a cell group.
    pub fn find_pin(&self, pin_name: &str) -> Option<&Group> {
        assert_eq!(self.name, "cell", "Must be a cell group.");
        self.find_group_by_arg("pin", pin_name)
    }
}

/// Parse a string of the form `"1.2, 1.3"` info a `Vec<f64>`.
pub fn parse_float_array(s: &str) -> Result<Vec<f64>, ParseFloatError> {
    // Strip away quotes.
    let s = s.strip_prefix('"').unwrap_or(s);
    let s = s.strip_suffix('"').unwrap_or(s);

    let splitted = s.split(',');
    splitted
        .map(|s| {
            // Strip whitespace.
            let s = s.strip_prefix(|c: char| c.is_whitespace()).unwrap_or(s);
            let s = s.strip_suffix(|c: char| c.is_whitespace()).unwrap_or(s);

            // Parse to float.
            f64::from_str(s)
        })
        .collect()
}

#[test]
fn test_parse_float_array() {
    let s = r#""1.2, 1.3, 1.4""#;
    let v = parse_float_array(s).unwrap();
    assert_eq!(v, vec![1.2, 1.3, 1.4])
}

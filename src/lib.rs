// Copyright (c) 2021-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Input and output library for the Liberty format.
//! The Liberty format is used to describe characteristics of standard-cell libraries.
//!
//! The liberty library is represented as nested [`Group`] structures.
//!
//! # Example
//!
//! Read a liberty file:
//! ```
//! use liberty_io;
//! use std::fs::File;
//! use std::io::BufReader;
//!
//! // Create a buffered reader for faster reading.
//! let f = File::open("./tests/data/freepdk45/gscl45nm.lib").expect("Failed to open file.");
//! let mut buf = BufReader::new(f);
//!
//! // Read the file.
//! let read_result = liberty_io::read_liberty_bytes(&mut buf);
//! // Print the parsed library or error value.
//! // dbg!(&read_result);
//! // Abort the program if the library could not be read.
//! let library_group = read_result.expect("Failed to read library!");
//!
//! // Access the content.
//! assert_eq!(&library_group.name, "library");
//! assert_eq!(&library_group.arguments[0].to_string(), "gscl45nm");
//!
//! // List all cell names. (There's only DFFNEGX1 in the provided example file.)
//! println!("Library cells:");
//! for group in &library_group.groups {
//!     if group.name == "cell" {
//!         println!("* {}", &group.arguments[0]);
//!     }
//! }
//!
//! // There's some utility functions for accessing the library structure.
//! // Find a cell group by it's name.
//! let dffnegx1 = library_group.find_cell("DFFNEGX1");
//! assert!(dffnegx1.is_some());
//! ```
//!
//! [`Group`]: ast::Group
//!

#![deny(missing_docs)]
// TODO: Remove those once this crate stabilizes.
#![allow(unused)]

mod ast;
mod ast_generated;
pub mod boolean;
mod parser;
mod parser_v2;
mod stream_lexer;
mod units;
pub mod util;

pub use ast::*;
pub use parser::{read_liberty_bytes, read_liberty_chars};
pub use units::*;
